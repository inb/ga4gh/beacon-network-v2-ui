/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.engine;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconNetworkRequestExecutor {

    @Inject 
    private BeaconNetworkEndpointsOracle oracle;

    private Client client;
    
    @PostConstruct
    public void init() {
        client = ClientBuilder.newBuilder()
                .executorService(Executors.newCachedThreadPool())
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(1, TimeUnit.MINUTES).build();
    }

    public List<BeaconInvocation> execute(HttpServletRequest req, String query) {

        final URI root;
        try {
            root = new URI(req.getScheme(), null, req.getServerName(), req.getServerPort(), req.getContextPath(), null, null);
        } catch (URISyntaxException ex) {
            return Collections.EMPTY_LIST;
        }

        final List<BeaconInvocation> invocations = new ArrayList();
                
        final Map<String, String> parameters = oracle.parseQuery(query);
        final List<String> paths = oracle.resolveTemplates(parameters);
        for (String path : paths) {
            final UriBuilder uri_builder = UriBuilder.fromUri(root.toString())
                    .path("v2.0.0").path(path);
            final Invocation.Builder builder = client.target(uri_builder).request();
            final int idx = path.indexOf('?');
            final BeaconInvocation invocation = new BeaconInvocation(null, idx < 0 ? path : path.substring(0, idx), null);
            builder.async().get(invocation);
            
            invocations.add(invocation);
        }
        
        return invocations;
    }
}
