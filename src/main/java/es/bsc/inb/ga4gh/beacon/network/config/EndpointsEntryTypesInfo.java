/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.config;

import es.bsc.inb.ga4gh.beacon.network.engine.EndpointsDefinitions;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Repchevsky
 */

@Named
@ApplicationScoped
public class EndpointsEntryTypesInfo {

    @Inject
    private EndpointsDefinitions endpoints_definitions;

    private Map<String, String> entry_types;

    @PostConstruct
    public void init() {
        entry_types = new HashMap();
        
        final Map<String, Map<String, String>> endpoints = endpoints_definitions.getEndpoints();
        for (Map<String, String> value : endpoints.values()) {
            entry_types.putAll(value);
        }
    }
    
    @Produces @Named("entry_types")
    public Map<String, String> getEntryTypes() {
        return entry_types;
    }
}
