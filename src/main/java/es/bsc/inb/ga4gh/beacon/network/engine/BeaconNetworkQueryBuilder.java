/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.engine;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.common.Pagination;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.requests.BeaconRequestQuery;
import jakarta.enterprise.context.ApplicationScoped;
import java.util.Map;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconNetworkQueryBuilder {
    
    
    public BeaconRequestQuery build(final Map<String,String> parameters) {
        final BeaconRequestQuery query = new BeaconRequestQuery();

        final Pagination pagination = buildPagination(parameters);
        query.setPagination(pagination);
        
        return query;
    }
    
    private Pagination buildPagination(Map<String,String> parameters) {
        Integer skip = null;
        Integer limit = null;
        
        final String skip_param = parameters.get("skip");
        if (skip_param != null) {
            try {
                skip = Integer.parseInt(skip_param);
            } catch(NumberFormatException ex) {}
        }

        final String limit_param = parameters.get("limit");
        if (limit_param != null) {
            try {
                limit = Integer.parseInt(limit_param);
            } catch(NumberFormatException ex) {}
        }
        
        if (skip != null || limit != null) {
            return new Pagination(skip, limit);
        }
        
        return null;
    }

}
