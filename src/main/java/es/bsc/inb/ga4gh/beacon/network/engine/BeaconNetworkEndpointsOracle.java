/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.engine;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Dmitry Repchevsky
 */

@ApplicationScoped
public class BeaconNetworkEndpointsOracle {

    @Inject @Named("endpoints_parameters")
    private Map<String, Set<String>> endpoints_parameters;

    public LinkedHashMap<String, String> parseQuery(String query) {
        final LinkedHashMap<String,String> parameters = new LinkedHashMap();
        
        if (query != null && !query.isEmpty()) {
            query = query.replaceAll("\\s+=\\s+", "=");
            final String[] params = query.split("[ ,;&]");

            for (String param : params) {
                final int idx = param.indexOf('=');
                if (idx < 0) {
                    parameters.put(param, "");
                } else {
                    parameters.put(param.substring(0, idx), param.substring(idx + 1));
                }
            }
        }

        return parameters;
    }

    public Map<String, String> guessEndpoints(Map<String, String> params) {
        return resolveTemplates(params, false);
    }

    public List<String> resolveTemplates(Map<String, String> params) {
        final Map<String, String> endpoints = resolveTemplates(params, true);
        return endpoints.values().stream().filter(p -> !p.contains("{")).collect(Collectors.toList());
    }

    private Map<String, String> resolveTemplates(Map<String, String> params, boolean addParams) {
        List<String> templates = getTemplates(params);
        
        final Map<String, String> endpoints = new HashMap();
        for (String template : templates) {
            final StringBuilder endpoint = new StringBuilder(template);
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (!entry.getValue().isEmpty()) {
                    final String param = "{" + entry.getKey() + "}";
                    int i = endpoint.indexOf(param);
                    if (i >= 0) {
                        do {
                            endpoint.replace(i, i + param.length(), entry.getValue());
                        } while((i = endpoint.indexOf(param, i + param.length())) > 0);
                    } else if (addParams) {
                        endpoint.append(endpoint.indexOf("?") < 0 ? "?" : "&");
                        endpoint.append(entry.getKey()).append("=").append(entry.getValue());
                    }
                }
            }
            endpoints.put(template, endpoint.toString());
        }
        return endpoints;
    }
    
    private List<String> getTemplates(Map<String, String> params) {
        final Set<String> names = params.keySet();
        final List<String> endpoints = new ArrayList();

        for (Map.Entry<String, Set<String>> entry : endpoints_parameters.entrySet()) {
            if (entry.getValue().containsAll(names)) {
                endpoints.add(entry.getKey());
            }
        }
        return endpoints;
    }

}
