/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.config;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.BeaconMap;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.configuration.Endpoint;
import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconMapResponse;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.parser.OpenAPIV3Parser;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.ObservesAsync;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.json.JsonString;
import jakarta.json.JsonValue;
import jakarta.servlet.ServletContext;
import java.io.Serializable;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Dmitry Repchevsky
 */

@Named("parameters_info")
@ApplicationScoped
public class RequestParametersInfo implements Serializable {

    private final static String DEFAULT_MODEL_ENDPOINTS = "BEACON-INF/default-model-endpoints.json";

    @Inject 
    private ServletContext ctx;
    
    @Inject
    private NetworkConfiguration network_configuration;
    
    @Inject @Named("entry_types")
    private Map<String, String> entry_types; 

    /**
     * The map that contains all beacons endpoints' parameters.
     * 
     * Map<'beaconId', Map<'url', Set<'param'>>>
     * Map<'org.progenetix.beacon', 
     *  Map<'/{id}/g_variants', ['allele']>>
     */
    private Map<String, Map<String, Set<String>>> parameters;
    
    /**
     * The map that contains merged endpoints' parameters.
     * There is no distinction between beacons. So this map is
     * basically the merge of all maps above.
     * 
     * Map<'url', Set<'param'>>
     * Map<'/{id}/g_variants', ['allele']>>
     */
    private Map<String, Set<String>> endpoints_parameters;

    public Map<String, Map<String, Set<String>>> getParameters() {
        return parameters;
    }

    @Produces @Named("endpoints_parameters")
    public Map<String, Set<String>> getEndpointsParameters() {
        return endpoints_parameters;
    }

    @PostConstruct
    public void init() {
        parameters = new HashMap();
        endpoints_parameters = new HashMap();

        loadEndpointParameters();
        loadDefaultEndpointParameters();
    }
    
    public void onEvent(@ObservesAsync NetworkConfigEvent event) {
        // do nothing
    }

    private void loadEndpointParameters() {

        final Map<String, BeaconMapResponse> maps = network_configuration.getMaps();
        final Map<String, String> endpoints = network_configuration.getEndpoints();
        for (Map.Entry<String, String> endpoint_entry : endpoints.entrySet()) {
            final URI beacon_network_uri;
            try {
                beacon_network_uri = URI.create(endpoint_entry.getValue());
            } catch(IllegalArgumentException ex) {
                continue;
            }
            final String beaconId = endpoint_entry.getKey();
            final BeaconMapResponse map_response = maps.get(beaconId);
            final BeaconMap map = map_response.getResponse();
            if (map != null) {
                final Map<String, Endpoint> endpoint_sets = map.getEndpointSets();
                if (endpoint_sets != null) {
                    for (Endpoint endpoint : endpoint_sets.values()) {
                        String openapiUrl = endpoint.getOpenAPIEndpointsDefinition();
                        if (openapiUrl != null) {
                            try {
                                if (!URI.create(openapiUrl).isAbsolute()) {
                                    // resolve relative url
                                    openapiUrl = beacon_network_uri.resolve(openapiUrl).toString();
                                }
                            } catch(IllegalArgumentException ex) {
                                continue;
                            }
                            final Map<String, Set<String>> endpoints_params = getParameters(openapiUrl);
                            if (!endpoints_params.isEmpty()) {
                                
                                // filter out all openapi endpoints not defined in the '/map'
                                endpoints_params.keySet().retainAll(entry_types.keySet());
                                
                                final Map<String, Set<String>> params = parameters.get(beaconId);
                                if (params == null) {
                                    parameters.put(beaconId, endpoints_params);
                                } else {
                                    params.putAll(endpoints_params);
                                }
                                
                                for (Map.Entry<String, Set<String>> endpoint_params : endpoints_params.entrySet()) {
                                    final Set<String> endpoint_param = endpoints_parameters
                                            .putIfAbsent(endpoint_params.getKey(), endpoint_params.getValue());
                                    if (endpoint_param != null) {
                                        endpoint_param.addAll(endpoint_params.getValue());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void loadDefaultEndpointParameters() {
        try (JsonReader reader = Json.createReader(ctx.getResourceAsStream(DEFAULT_MODEL_ENDPOINTS))) {
            final JsonObject o = reader.readObject();
            for (Map.Entry<String, JsonValue> entry : o.entrySet()) {
                if (entry.getValue().getValueType() == JsonValue.ValueType.STRING) {
                    final String url = ((JsonString)entry.getValue()).getString();
                    final Map<String, Set<String>> endpoints_params = getParameters(url);
                    if (!endpoints_params.isEmpty()) {
                        // filter out all openapi endpoints not defined in the '/map'
                        endpoints_params.keySet().retainAll(entry_types.keySet());
                        for (Map.Entry<String, Set<String>> params_entry : endpoints_params.entrySet()) {
                            final Set<String> endpoint_param = endpoints_parameters.putIfAbsent(params_entry.getKey(), params_entry.getValue());
                            if (endpoint_param != null) {
                                endpoint_param.addAll(params_entry.getValue());
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(RequestParametersInfo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Map<String, Set<String>> getParameters(String openapiUrl) {
        final Map<String, Set<String>> map = new HashMap();
        
        final OpenAPI openAPI = new OpenAPIV3Parser().read(openapiUrl);
        if (openAPI != null) {
            final Paths paths = openAPI.getPaths();
            if (paths != null) {
                for (Map.Entry<String, PathItem> entry : paths.entrySet()) {
                    final PathItem path = entry.getValue();
                    final Operation operation = path.getGet();
                    if (operation != null) {
                        final List<Parameter> params = operation.getParameters();
                        if (params != null && !params.isEmpty()) {
                            final Set<String> names = params.stream()
                                    .filter(p -> p.getName() != null && !p.getName().isEmpty())
                                    .map(p -> p.getName())
                                    .collect(Collectors.toSet());
                            map.put(entry.getKey(), names);
                        }
                    }
                }
            }
        }
        return map;
    }
}
