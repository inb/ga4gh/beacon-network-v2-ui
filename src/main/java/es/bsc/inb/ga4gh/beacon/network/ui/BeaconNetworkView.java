/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.ui;

import es.bsc.inb.ga4gh.beacon.network.engine.BeaconNetworkEndpointsOracle;
import jakarta.annotation.PostConstruct;
import jakarta.faces.event.AjaxBehaviorEvent;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Dmitry Repchevsky
 */

@Named("bn")
@ViewScoped
public class BeaconNetworkView implements Serializable {
    
    private String query;
    private String skip;
    private String limit;
    
    private List<String> hints;
    private Map<String, String> params;
    private Map<String, String> endpoints;
    
    @Inject 
    private BeaconNetworkEndpointsOracle oracle;

    @Inject
    private WebSocketResultProcessor processor;
    
    @PostConstruct
    public void init() {
        hints = new ArrayList();
        params = new HashMap();
        endpoints = oracle.guessEndpoints(params);
    }

    public String getQuery() {
        return query;
    }
    
    public void setQuery(String query) {
        this.query = query;
    }

    public String getSkip() {
        return skip;
    }
    
    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getLimit() {
        return limit;
    }
    
    public void setLimit(String limit) {
        this.limit = limit;
    }
    
    public Map<String, String> getParams() {
        return params;
    }

    public Map<String, String> getEndpoints() {
        return endpoints;
    }

    public String getTemplate(String endpoint) {
        final Set<Map.Entry<String, String>> set = endpoints.entrySet();
        final String template = set.stream()
                .filter(e -> e.getValue().equals(endpoint))
                .findFirst().orElse(Map.entry("", endpoint)).getKey();

        return template;
    }

    public void analyze(AjaxBehaviorEvent event) {
        params = getParameters(query);
        endpoints = oracle.guessEndpoints(params);
    }

    public void submit(AjaxBehaviorEvent event) {
        final HttpServletRequest request = (HttpServletRequest)event.getFacesContext().getExternalContext().getRequest();
        final Map<String, String> parameters = getParameters(query);
        final String q = parameters.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("&"));
        
        processor.submit(request, q);
    }
    
    private Map<String, String> getParameters(String query) {
        final Map<String, String> parameters = oracle.parseQuery(query);

        if (skip != null && !skip.isEmpty()) {
            parameters.put("skip", skip);
        }

        if (limit != null && !limit.isEmpty()) {
           parameters.put("limit", limit);
        }
        return parameters;
    }
}
