/**
 * *****************************************************************************
 * Copyright (C) 2022 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 * *****************************************************************************
 */

package es.bsc.inb.ga4gh.beacon.network.ui;

import es.bsc.inb.ga4gh.beacon.framework.model.v200.responses.BeaconResponse;
import es.bsc.inb.ga4gh.beacon.network.engine.BeaconInvocation;
import es.bsc.inb.ga4gh.beacon.network.engine.BeaconNetworkRequestExecutor;
import es.bsc.inb.ga4gh.beacon.network.engine.BeaconNetworkResponseBuilder;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.faces.push.Push;
import jakarta.faces.push.PushContext;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.core.Response;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Repchevsky
 */

@ViewScoped
@Named("processor")
public class WebSocketResultProcessor implements Serializable {

    @Inject
    @Push(channel = "search_query_result")
    private PushContext push;
    
    @Inject
    private BeaconNetworkRequestExecutor executor;
    
    @Inject
    private BeaconNetworkResponseBuilder response_builder;

    private List<BeaconInvocation> invocations;
    private Map<String, BeaconResponse> responses;

    public Map<String, BeaconResponse> getResponses() {
        return responses;
    }
    
    @PostConstruct
    public void init() {
        invocations = new ArrayList();
        responses = new HashMap();
    }

    /**
     * Stop all threads that are executing Beacons calls on view destroy (i.g. page reload).
     */
    @PreDestroy
    public void destroy() {
        cancel();
    }

    /**
     * Submit the (URL) query to this (Beacono Network) REST endpoint.
     * 
     * @param req the current HTTP request (needed to construct the request)
     * @param query the URL query parameters to be proxied (param1=a&param2=b...)
     */
    public void submit(HttpServletRequest req, String query) {
        cancel();

        invocations = executor.execute(req, query);
        for (BeaconInvocation invocation : invocations) {
            invocation.future.whenComplete((response, th) -> complete(invocation, response, th));
        }
    }

    /**
     * Cancel all currently active threads that are calling Beacons
     */
    public void cancel() {
        for (BeaconInvocation invocation : invocations) {
            if (!invocation.future.isDone()) {
                invocation.future.cancel(true);
            }
        }
        responses.clear();
        invocations.clear();
    }

    public boolean isRunning() {
        return !invocations.isEmpty() && invocations.size() > responses.size();
    }

    private void complete(BeaconInvocation invocation, Response response, Throwable th) {
        if (!invocation.future.isCancelled()) {
            final BeaconResponse result = response_builder.getResultset(invocation);
            responses.put(invocation.template, result);
            push.send(invocation.template);
        }
    }
}
